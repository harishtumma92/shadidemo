package com.example.shadi

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.shadi.data.Profile
import com.example.test2.adapter.MyAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.single_cell.*

class MainActivity : AppCompatActivity() {
    private lateinit var mainVM: MainVM
    private  lateinit var adapter: MyAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainVM=  ViewModelProvider(this, MainVM.MainVMFactory(application,(application as MyApplication).repo))
                .get(MainVM::class.java)

        adapter= MyAdapter(this,mainVM)

        val lm =
                StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recycler_view.setLayoutManager(lm)
        recycler_view.setItemAnimator(DefaultItemAnimator())
        recycler_view.setAdapter(adapter)

        //  recycler_view.setBackgroundColor(R.color.colorPrimary)



        observer()
    }

    private fun observer() {
        mainVM.profileList.observe(this, object : Observer<List<Profile>>{
            override fun onChanged(t: List<Profile>?) {
                adapter.refreshList(t)
            }
        });
      /*  mainVM.empList.observe(this,object : Observer<List<Employee>> {
            override fun onChanged(t: List<Employee>?) {

                Toast.makeText(this@MainActivity,"fggdsgdg", Toast.LENGTH_LONG).show()
                adapter.refreshList(t)
            }
        })*/
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)
    }

    companion object {
        const val ACCEPT="Accepted"
        const val DECLINE="Declined"
    }
}