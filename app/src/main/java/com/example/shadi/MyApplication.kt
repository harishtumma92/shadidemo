package com.example.shadi

import android.app.Application
import com.example.shadi.data.MyDatabaseManager
import com.example.test2.repository.ProfileRepo

class MyApplication :Application() {
     lateinit var repo:ProfileRepo
    override fun onCreate() {
        super.onCreate()
        repo=ProfileRepo(MyDatabaseManager.getMyDataBase(this))
    }
}