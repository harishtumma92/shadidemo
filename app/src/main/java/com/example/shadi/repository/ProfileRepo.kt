package com.example.test2.repository

import androidx.lifecycle.LiveData
import com.example.shadi.data.MyRoomDatabase
import com.example.shadi.data.Profile
import com.example.shadi.webservice.model.response.Response
import com.example.shadi.webservice.model.response.ResultsItem
import com.example.test2.webservice.MyApi

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProfileRepo(private val  myRoomDataBase: MyRoomDatabase)
 {

     lateinit var profilList : LiveData<List<Profile>>
     init {
         GlobalScope.launch {
             profilList = myRoomDataBase.profileDao.getProfile()
         }
     }


     suspend fun update(data: ResultsItem){
         myRoomDataBase.profileDao.updateProfile(
             Profile(0,data?.name?.first+" "+ data?.name?.last,
                 data?.gender.toString(),
                 data?.dob?.age.toString(),
                 data?.picture?.medium.toString(),
                 data?.cell.toString(),
                 ""
             )
         )

     }

     suspend fun update(data:  Profile){
         withContext(Dispatchers.IO) {
             myRoomDataBase.profileDao.updateProfile(data)
         }
     }

     suspend fun refreshData() {
         withContext(Dispatchers.IO) {
             val playlist = MyApi.retrofitService.getProfile()
             myRoomDataBase.profileDao.insertAll(parseData(playlist))
         }
     }


     private fun parseData(response: Response):ArrayList<Profile>
     {

         var pList=response.results
         var list= ArrayList<Profile>()
         for (dataItem in pList!!)
         {

                list.add(
                    Profile(0,dataItem?.name?.first+" "+ dataItem?.name?.last,
                        dataItem?.gender.toString(),
                        dataItem?.dob?.age.toString(),
                        dataItem?.picture?.medium.toString(),
                        dataItem?.cell.toString(),
                        ""
                    )

                )

         }
         return  list
     }

 }