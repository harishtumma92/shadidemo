package com.example.shadi

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.*
import com.example.shadi.data.MyDatabaseManager
import com.example.shadi.data.Profile
import com.example.shadi.webservice.model.response.ResultsItem
import com.example.test2.repository.ProfileRepo
import com.example.test2.webservice.MyApi
import kotlinx.coroutines.Dispatchers

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

import java.io.IOException

class MainVM( val application: Application,profile: ProfileRepo) :ViewModel()
{
    private val profileRepo = profile//ProfileRepo(MyDatabaseManager.getMyDataBase(application))
    val profileList:LiveData<List<Profile>>


    //var application2:Application

   /* private val _profileList = MutableLiveData<com.example.shadi.webservice.model.response.Response>()

      val playlist: LiveData<com.example.shadi.webservice.model.response.Response>
        get() = _profileList*/



    init {
        //this.application2=application
        refreshDataFromRepository()
       profileList= profileRepo.profilList

        //Toast.makeText(application,"sdjf",Toast.LENGTH_SHORT).show()
    }


    fun updateData(resultsItem: ResultsItem) {
        viewModelScope.launch {
                profileRepo.update(resultsItem)
        }
    }

    suspend fun updateData(data:  Profile){
       // viewModelScope.launch {
                withContext(Dispatchers.IO)
                {
                    profileRepo.update(data)
                }
       // }
    }



    fun refreshDataFromNetwork(){

        viewModelScope.launch {
            try {
                var res=  MyApi.retrofitService.getProfile()
                //Toast.makeText(application,res.status.toString(),Toast.LENGTH_SHORT).show()

            }
            catch (e:Exception)
            {
                delay(5000)
            }
               }





  /*      MarsApi.retrofitService.getProperties().enqueue(
            object: Callback<com.example.test2.webservice.model.response.Response> {
                override fun onResponse(call: Call<com.example.test2.webservice.model.response.Response>, response: Response<com.example.test2.webservice.model.response.Response>) {
                    //_response.value=response.body()
                    Toast.makeText(application,response.body()?.status.toString(),Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(call: Call<com.example.test2.webservice.model.response.Response>, t: Throwable) {
                    //_response.value=t.message
                    Toast.makeText(application,t.message,Toast.LENGTH_SHORT).show()
                }
            })
*/

    }



    private fun refreshDataFromRepository() {
        viewModelScope.launch {
            try {
                profileRepo.refreshData()

            } catch (networkError: IOException) {

            }
        }
    }

    class MainVMFactory(val app: Application,val profile: ProfileRepo):ViewModelProvider.Factory {
        //val repository =repository
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MainVM(app,profile) as T
        }
    }


}