package com.example.test2.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.shadi.MainActivity
import com.example.shadi.MainVM
import com.example.shadi.R
import com.example.shadi.data.Profile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MyAdapter(val context:Context,val mainVM: MainVM):RecyclerView.Adapter<MyAdapter.MyviewHolder>(){

     var list: ArrayList<Profile>?
    init {
        list=ArrayList()
    }

   fun refreshList(t: List<Profile>?)
    {
       list?.clear()
       list?.addAll(t!!)
       this.notifyDataSetChanged()
   }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdapter.MyviewHolder {
        var view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cell, parent, false);
        return MyviewHolder(view)
    }

    override fun onBindViewHolder(holder: MyAdapter.MyviewHolder, position: Int) {

        try {
            holder.name?.text = list?.get(position)?.name
            holder.age?.text = list?.get(position)?.age
            holder.contact?.text = list?.get(position)?.contact
            try {
                Glide.with(context).load(list?.get(position)?.profile_image)
                    .centerCrop()
                    .into(holder?.image!!);
              } catch (e: Exception)
              { }

            holder.accept?.setOnClickListener {
                var data=list?.get(position)
                data?.status=MainActivity.ACCEPT
                if (data != null) {
                    GlobalScope.launch {
                        mainVM.updateData(data)
                    }
                };
            }

            holder.decline?.setOnClickListener {
                var data=list?.get(position)
                data?.status=MainActivity.DECLINE
                if (data != null) {
                    GlobalScope.launch {
                        withContext(Dispatchers.IO)
                        {
                            mainVM.updateData(data)
                           // mainVM.
                        }
                    }
                };
            }

            if(list?.get(position)?.status==null || list?.get(position)?.status.equals(""))
            {
                holder.status?.visibility=View.GONE
                holder.accept?.visibility=View.VISIBLE
                holder.decline?.visibility=View.VISIBLE
            }
            else{
                holder.status?.visibility=View.VISIBLE
                holder.accept?.visibility=View.GONE
                holder.decline?.visibility=View.GONE
                 holder.status?.setText(list?.get(position)?.status.toString())
            }





        } catch (e:Exception)
        {}
    }

    override fun getItemCount(): Int {
            return list?.size!!
    }

    class MyviewHolder(view:View):RecyclerView.ViewHolder(view) {
        //var mailLayout: LinearLayout? = view.findViewById(R.id.main_layout)
        var image: ImageView? = view.findViewById(R.id.image)
        var name: TextView? = view.findViewById(R.id.name)
        var age: TextView? = view.findViewById(R.id.age)
        var contact: TextView? = view.findViewById(R.id.contact)
        var accept: Button? = view.findViewById(R.id.accept_btn)
        var decline: Button? = view.findViewById(R.id.decline_btn)
        var status: TextView? = view.findViewById(R.id.status_txt)

    }

}