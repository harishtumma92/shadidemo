package com.example.shadi.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Profile::class], version = 1)
abstract class MyRoomDatabase: RoomDatabase() {
    abstract val profileDao: ProfileDao
}