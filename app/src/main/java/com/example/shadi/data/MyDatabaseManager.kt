package com.example.shadi.data

import android.app.Application
import androidx.room.Room

object MyDatabaseManager {

    var db: MyRoomDatabase? =null
    init {
    }

    fun getMyDataBase(applicationContext: Application): MyRoomDatabase
    {
        if(db !=null)
        {
            return db as MyRoomDatabase
        }
        db = Room.databaseBuilder(applicationContext, MyRoomDatabase::class.java, "myShadi.db")
            .build()
        return db as MyRoomDatabase
    }
}