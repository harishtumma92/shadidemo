package com.example.shadi.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity()
class Profile(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val name:String,
    val gender:String,
    val age:String,
    val profile_image:String,
    val contact:String,
    var status:String
    ) :Serializable


