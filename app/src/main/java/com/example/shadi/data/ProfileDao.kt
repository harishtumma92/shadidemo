package com.example.shadi.data

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface ProfileDao {


    @Query("select * from Profile")
    fun getProfile(): LiveData<List<Profile>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll( profile : List<Profile>)

    @Update
    fun updateProfile(vararg profile: Profile)
}